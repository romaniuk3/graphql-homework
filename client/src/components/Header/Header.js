import React from 'react';
import { Dropdown } from "semantic-ui-react";
// import './style.css';

const Header = props => {
  const { sortOptions, setOrder, setFilter } = props;

  const onSelect = (e, data) => {
    setOrder(data.value);
  }

  return (
    <div className="header container">
      <div>
        Sort by: {' '}
        <Dropdown
          options={sortOptions}
          defaultValue={sortOptions[0].value}
          onChange={onSelect}
        />
      
      </div>
      <input onChange={e => setFilter(e.target.value)}/>
    </div>
  )
}

export default Header;